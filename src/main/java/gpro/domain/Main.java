package gpro.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

    public class Main {


        public static void main(String[] args) throws IOException {
            ArrayList<User> users = new ArrayList<>();
            File file = new File("db.txt");
            Scanner fsc = new Scanner(file);

            while (fsc.hasNext()) {
                int id = fsc.nextInt();
                String name = fsc.next();
                String surname = fsc.next();
                String username = fsc.next();
                String password = fsc.next();
                User user = new User(id, name, surname, username, password);
                users.add(user);
            }
            Scanner sc = new Scanner(System.in);
            System.out.println("New user creation..");
            System.out.println("Enter name:");
            String name = sc.next();
            System.out.println("Enter surname:");
            String surname = sc.next();
            System.out.println("Enter username:");
            String username = sc.next();
            while (!checkUsername(username)) {
                System.out.println("Error:Username is in use. Enter another username:");
                username = sc.next();
            }
            System.out.println("Enter password:");
            String password = (sc.next());
            while (!Password.is_Valid_Password(password)) {
                System.out.println("Your password is invalid. Password must contain uppercase and lowercase letters,digits and must be more than 9 symbols:");
                password = sc.next();
            }
            ;

            User newUser = new User(name, surname, username, password);
            users.add(newUser);

            for (User user : users) {
                System.out.println(user);
            }
            String content = "";
            for (User user : users) {
                content += user.getId() + " " + user.getName() + " " + user.getSurname() + " " + user.getUsername() + " " + user.getPasswordStr() + "\n";
            }
            Files.write(Paths.get("db.txt"), content.getBytes());
            MyApplication application = new MyApplication();
            System.out.println("An application is about to start..");
            try {
                application.start();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }



        private static boolean checkUsername(String username) {
            for (User user = new User) {
                if (user.getUsername().equals(username)) return false;
            }
            return true;
        }


    }



package gpro.domain;

public class User {
    // id (you need to generate this id by static member variable)
    // name, surname
    // username
    // password
    private int id;
    private static int id_gen = 0;
    public String name;
    public static String surname;
    public String username;
    public Password password;

    public User(int id) {
        generateId();
    }


    public User(int id, String name, String surname, String username, String password) {
        super.setPasswordStr(password);
        generateId();
        setName(name);
        setSurname(surname);
        setUsername(username);
    }

    public User(String name, String surname, String username, String password) {
        super.setPasswordStr(password);
        setName(name);
        setSurname(surname);
        setUsername(username);
    }

    public User() {
    }

    public void generateId() {
        id = id_gen++;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private boolean checkUsername(String username) {

        return false;
    }

    @Override
    public String toString() {
        return name+" "+surname+" "+username+" "+super.toString();
    }

}

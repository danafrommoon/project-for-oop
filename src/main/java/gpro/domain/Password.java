package gpro.domain;

public class Password {
    // passwordStr // it should contain uppercase and lowercase letters and digits
    // and its length must be more than 9 symbols
    private String passwordStr;
    public static final int PASSWORD_LENGTH =9 ;
    public Password(String passwordStr){
        setPasswordStr(passwordStr);
    }

    public Password() {
    }

    public String getPasswordStr(){
        return passwordStr;
    }
    public void setPasswordStr(String passwordStr){
        this.passwordStr=passwordStr;
    }
    public static boolean is_Valid_Password(String passwordStr) {

        if (passwordStr.length() < PASSWORD_LENGTH) return false;

        int charCount = 0;
        int numCount = 0;
        for (int i = 0; i < passwordStr.length(); i++) {

            char ch = passwordStr.charAt(i);

            if (is_Numeric(ch)) numCount++;
            else if (is_Letter(ch) || is_Letter1(ch)) charCount++;
            else return false;
        }


        return (charCount >= 2 && numCount >= 2);
    }

    public static boolean is_Letter(char ch) {
        ch = Character.toUpperCase(ch);
        return (ch >= 'A' && ch <= 'Z');
    }
    public static boolean is_Letter1(char ch) {
        ch = Character.toLowerCase(ch);
        return (ch >= 'a' && ch <= 'z');
    }

    public static boolean is_Numeric(char ch) {

        return (ch >= '0' && ch <= '9');
    }

    @Override
    public String toString() {
        return passwordStr;
    }
}

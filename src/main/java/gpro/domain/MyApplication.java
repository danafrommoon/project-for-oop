package gpro.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApplication extends User{
    // users - a list of users
    private ArrayList<User> users;
    private Scanner sc = new Scanner(System.in);
    private User signedUser;
    public MyApplication() throws FileNotFoundException {
        getName();getSurname();getUsername();
        password.getPasswordStr();
        users = new ArrayList<>();
        fillUsers();
    }
    private void fillUsers() throws FileNotFoundException{
        File file = new File("db.txt");
        Scanner fileScanner = new Scanner(file);
    }
    private void addUser(User user) {
        users.add(user);
    }

    private void menu() {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            }
            else {
                userProfile();
            }
        }
    }

    private void userProfile() {
        for (User user : users) {
            signedUser = user;
        }
        System.out.println("You are signed in.");
        System.out.println("Exit");
    }

    private void logOff() {
        signedUser = null;
    }

    private void authentication() {
        // sign in
        // sign up
        for (User user : users){

            signIn(user.getUsername(),user.getPasswordStr());
        }

    }

    private void signIn(String username, Password password) {
        for(User user: users){
            if(user.getUsername().equals(username) && user.getPasswordStr().equals(password)) userProfile();
        }
        System.out.println("Invalid username or password!");
    }


    private void signUp(String name, String surname, String username, String password) {
        User user = new User(name, surname, username, password);
        users.add(user);
        user.setName(name);
        user.setSurname(surname);
        user.setPasswordStr(password);
    }

    public void start() throws FileNotFoundException {
        File file = new File("db.txt");
        Scanner fileScanner = new Scanner(file);
        // fill userlist from db.txt

        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }

        // save the userlist to db.txt
    }
    private static void newPage(){
        for (int i=0;i<15;i++){
            System.out.println();
        }
    }
    private void saveUserList() {
        String content = "";
        for (User user: users){
            content += user.getId() + " "+ user.getName() + " " + user.getSurname() + " " + user.getUsername() + " " + user.getPasswordStr() + "\n";
        }
        try {
            Files.write(Paths.get("db.txt"), content.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<User> getUsers() {
        return users;
    }
}


